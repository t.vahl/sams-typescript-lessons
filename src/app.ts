declare const dragulas;
declare const initDragula: (...args: any[]) => void;

$(document).ready(function() {
    prepareAllDragulas();
    new Greeter("Starting TDD").greet();
})

function prepareAllDragulas() {
    if(Array.isArray(dragulas)) {
        for(const dragula of dragulas) {
            initDragula(dragula);
        }
        new Greeter().greet();
    } else {
        throw Error("no array");
    }
}


class Greeter {
    public constructor(
        public greeting: string = "Hello World"
    ) {}

    public greet() {
        console.log(this.greeting);
    }
}

function checkIsGreeter(candidate: any): candidate is Greeter {
    return candidate.hasOwnProperty("greeting")
        && candidate.hasOwnProperty("greet");
}

const somethingGreeterLike: Record<string, any> = {
    greeting: "Clean code ftw",
    greet: () => alert(this.greeting)
};

if(checkIsGreeter(somethingGreeterLike)) {
    doGreet(somethingGreeterLike);
}

function doGreet(greeter: Greeter) {
    greeter.greet();
}

// function parkingPosition() {    
//     $(".search").keyup(function () {
//         const searchTerm = $(this).val();
//         $(this).parent().find("li").each(function () {
//             if($(this).text().includes(searchTerm)) {
//                 $(this).show();
//             } else {
//                 $(this).hide();
//             }
//         });
//     });
// }
var _this = this;
$(document).ready(function () {
    prepareAllDragulas();
    new Greeter("Starting TDD").greet();
});
function prepareAllDragulas() {
    if (Array.isArray(dragulas)) {
        for (var _i = 0, dragulas_1 = dragulas; _i < dragulas_1.length; _i++) {
            var dragula = dragulas_1[_i];
            initDragula(dragula);
        }
        new Greeter().greet();
    }
    else {
        throw Error("no array");
    }
}
var Greeter = /** @class */ (function () {
    function Greeter(greeting) {
        if (greeting === void 0) { greeting = "Hello World"; }
        this.greeting = greeting;
    }
    Greeter.prototype.greet = function () {
        console.log(this.greeting);
    };
    return Greeter;
}());
function checkIsGreeter(candidate) {
    return candidate.hasOwnProperty("greeting")
        && candidate.hasOwnProperty("greet");
}
var somethingGreeterLike = {
    greeting: "Clean code ftw",
    greet: function () { return alert(_this.greeting); }
};
if (checkIsGreeter(somethingGreeterLike)) {
    doGreet(somethingGreeterLike);
}
function doGreet(greeter) {
    greeter.greet();
}
// function parkingPosition() {    
//     $(".search").keyup(function () {
//         const searchTerm = $(this).val();
//         $(this).parent().find("li").each(function () {
//             if($(this).text().includes(searchTerm)) {
//                 $(this).show();
//             } else {
//                 $(this).hide();
//             }
//         });
//     });
// }
